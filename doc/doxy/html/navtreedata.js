var NAVTREE =
[
  [ "Dev PML", "index.html", [
    [ "Clases", "annotated.html", [
      [ "Lista de clases", "annotated.html", "annotated_dup" ],
      [ "Índice de clases", "classes.html", null ],
      [ "Jerarquía de la clase", "hierarchy.html", "hierarchy" ],
      [ "Miembros de las clases", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';