package com.cubex.pml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class Utils {
	
	public static String tranformString(String text, boolean firstUpper, boolean otherUpper, boolean inside, String separator) {
		StringBuffer sb = new StringBuffer();
		sb.ensureCapacity(text.length()*2);
		
		boolean lower = Character.isLowerCase(text.charAt(0));
		boolean first = true;
		boolean nextWord = true;
		char res = '\0';
		char last = '\0';
		for(char c : text.toCharArray()) {
			boolean _lower = Character.isLowerCase(c);
			
			if(Character.isAlphabetic(c) || Character.isDigit(c)) {
				if(first) {
					res = firstUpper ? Character.toUpperCase(c) : Character.toLowerCase(c);
					sb.append(res);
					first = false;
				} else {
					if((lower != _lower || nextWord) && !Character.isUpperCase(last)) {
						res = otherUpper ? Character.toUpperCase(c) : Character.toLowerCase(c);
						if(separator != null)
							sb.append(separator);
						sb.append(res);
					}
					else {						
						res = inside ? Character.toUpperCase(c) : Character.toLowerCase(c);
						sb.append(res);
					}
				}
				nextWord = false;
			}
			else
				nextWord = true;
			
			lower = _lower;
			last = c;
		}
		
		return sb.toString();
	}

	public static String readTextFile(File file) {
		BufferedReader b;
		String res = "";
		
		try {
			res = "";
			String cadena;
			FileReader fr = new FileReader(file);
			b = new BufferedReader(fr);
			while ((cadena = b.readLine()) != null) {
				res += (cadena + '\n');
			}
			
			if(!res.isEmpty())
				res = res.substring(0, res.length() - 1);

			b.close();
		} catch(Throwable t) {
			res = null;
		}
		
		return res;
	}
	
	public static String readTextFile(String path) {
		return readTextFile(new File(path));
	}

	public static Collection<String> readLineTextFile(File file) {
		BufferedReader b;
		List<String> res = new LinkedList<String>();
		
		try {
			String cadena;
			FileReader fr = new FileReader(file);
			b = new BufferedReader(fr);
			while ((cadena = b.readLine()) != null)
				res.add(cadena);

			b.close();
		} catch(Throwable t) {
			res = null;
		}
		
		return res;
	}

	public static Collection<String> readLineTextFile(String path) {
		return readLineTextFile(new File(path));
	}

	public static boolean saveTextFile(String path, String text) {
		return saveTextFile(path, text, "UTF-8");
	}
	
	public static boolean saveTextFile(String path, String text, String encoding) {
		try {
			File dir = new File(path).getParentFile();
			if(!dir.exists())
				dir.mkdirs();
			
			PrintWriter out = new PrintWriter(path, encoding);
			out.print(text);
			out.close();
		} catch(Throwable e) {
			return false;
		}
		
		return true;
	}
	
	public static String tabs(int num) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<num; i++)
			sb.append("\t");
		
		return sb.toString();
	}
	
	
	public static List<File> scanFolder(String folder) {
		List<File> files = new LinkedList<File>();

		try {
			File ffolder = new File(folder);
			File[] afiles = ffolder.listFiles();
			if(afiles != null) {
				for(File file : afiles) {
					if(file.isDirectory())
						files.addAll(scanFolder(folder + "/" + file.getName()));
					else
						files.add(file);
				}
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
		
		return files;
	}
}

