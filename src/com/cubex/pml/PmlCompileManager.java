package com.cubex.pml;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Parser;

import com.cubex.pml.tags.CaseTag;
import com.cubex.pml.tags.CatchTag;
import com.cubex.pml.tags.CustomTag;
import com.cubex.pml.tags.DeclareTag;
import com.cubex.pml.tags.DefaultTag;
import com.cubex.pml.tags.ElseIfTag;
import com.cubex.pml.tags.ElseTag;
import com.cubex.pml.tags.ExecTag;
import com.cubex.pml.tags.ForEachTag;
import com.cubex.pml.tags.ForLoopTag;
import com.cubex.pml.tags.IfTag;
import com.cubex.pml.tags.ImportTag;
import com.cubex.pml.tags.IncludeTag;
import com.cubex.pml.tags.ModelTag;
import com.cubex.pml.tags.OutTag;
import com.cubex.pml.tags.PutTag;
import com.cubex.pml.tags.SetTag;
import com.cubex.pml.tags.SwitchTag;
import com.cubex.pml.tags.TileTag;
import com.cubex.pml.tags.TilesTag;
import com.cubex.pml.tags.TryTag;
import com.cubex.pml.tags.WhileTag;

/**
 * Clase encargada de gestionar la ejecuci�n y compilaci�n de los script PMLs en tiempo de desarrollo. Esta clase es la responsable de
 * compilar los script PMLs cuando sea necesario y de ejecutar los scripts java que se le diga.
 *
 * @nosubgrouping
 */
public class PmlCompileManager {

	private File rootView;
	private File rootPackage;
	private File rootSources;
	private String layoutExtension;
	private String target;
	private Map<String, Long> viewsModified;
	private Map<String, Pml> views;
	private Map<String, Tag> tags;
	private boolean alwaysCompile;
	private int count;
	
	/** @name Constructores
	 */
	///@{
	/**
	 * Contructor b�sico, con extensi�n de layouts ".layout".
	 * @param rootView Carpeta raiz de vistas, donde se encuentran los scripts PMLs.
	 * @param rootPackage Paquete raiz donde se guardar�n los resultados de compilar los scripts.
	 * @param rootSources Carpeta raiz de los c�digos fuentes de su proyecto en su IDE.
	 * @throws Exception Puede ser lanzada por alg�n error con las rutas especificadas.
	 */
	public PmlCompileManager(String rootView, String rootPackage, String rootSources) throws Exception {
		this(rootView, rootPackage, rootSources, "layout");
	}
	
	/**
	 * Contructor con extensi�n de scripts PMLS layouts.
	 * @param rootView Carpeta raiz de vistas, donde se encuentran los scripts PMLs.
	 * @param rootPackage Paquete raiz donde se guardar�n los resultados de compilar los scripts.
	 * @param rootSources Carpeta raiz de los c�digos fuentes de su proyecto en su IDE.
	 * @param layoutExtension Especifica la extensi�n de los archivos layout, los cuales se ejecutar�n enteros y no como fragmentos.
	 * @throws Exception Puede ser lanzada por alg�n error con las rutas especificadas.
	 */
	public PmlCompileManager(String rootView, String rootPackage, String rootSources, String layoutExtension) throws Exception {
		super();
		this.rootView = new File(rootView);
		this.rootPackage = new File(rootPackage);
		this.rootSources = new File(rootSources);
		this.layoutExtension = layoutExtension;
		
		target = URLDecoder.decode(PmlCompileManager.class.getResource("/").toURI().toURL().toString().substring(6), "UTF-8");
		viewsModified = new HashMap<String, Long>();
		views = new Views();
		tags = new HashMap<String, Tag>();
		alwaysCompile = true;
		count = 0;
	}
	///@}

	
	/** @name Operaciones b�sicas
	 */
	///@{
	/**
	 * Establece la opci�n de compilar cada vez que se ejecute una vista, y no cuando se haya modificado su script.
	 * @param alwaysCompile Si es true cada vez que se ejecute una vista se compilar� antes el script, sino solo se compilar� cuando
	 * el archivo PML se haya modificado.
	 */
	public void setAlwaysCompile(boolean alwaysCompile) {
		this.alwaysCompile = alwaysCompile;
	}
	
	/**
	 * A�ade una etiqueta al compilador.
	 * @param tag Etiqueta.
	 */
	public void addTag(Tag tag) {
		tags.put(tag.getName(), tag);
	}
	
	/**
	 * A�ade las etiquetas est�ndar PML al compilador.
	 */
	public void addStandardTags() {
		addTag(new IfTag());
		addTag(new OutTag());
		addTag(new DeclareTag());
		addTag(new SetTag());
		addTag(new ExecTag());
		addTag(new ModelTag());
		addTag(new WhileTag());
		addTag(new ForLoopTag());
		addTag(new ForEachTag());
		addTag(new ElseIfTag());
		addTag(new ElseTag());
		addTag(new ImportTag());
		addTag(new TryTag());
		addTag(new CatchTag());
		addTag(new SwitchTag());
		addTag(new CaseTag());
		addTag(new DefaultTag());
		addTag(new TilesTag());
		addTag(new PutTag());
		addTag(new TileTag());
		addTag(new IncludeTag());
	}
	
	/**
	 * Ejecuta un script PML, el cual compilar� si fue modificado desde su �ltima ejecuci�n.
	 * @param view Nombre de la vista que se quiere ejecutar, la cual es una ruta relativa a la raiz de vistas.
	 * @param model Conjunto de variables globales que pasarle a la vista.
	 * @return Vista resultante
	 * @throws Exception Excpciones m�ltiples.
	 */
	public String exec(String view, Map<String, Object> model) throws Exception {
		Pml pml = views.get(view);
		if(pml != null)
			return pml.exec(model, views, null);
		
		return null;
	}
	
	/**
	 * Especifica la carptea raiz a escanear donde se especifican las etiquetas personalizadas.
	 * @param folder Carpeta raiz a escanear donde se especifican las etiquetas personalizadas.
	 */
	public void addCustomTags(String folder) {
		for(File tag : Utils.scanFolder(folder))
			addTag(new CustomTag(Utils.readTextFile(tag)));
	}
	
	/**
	 * Compila los script que no hayan sido compilados.
	 */
	public void compileAll() {
		Set<String> pml = new HashSet<String>();
		Set<String> clazz = new HashSet<String>();
		
		for(File file : Utils.scanFolder(rootView.getPath()))
			pml.add(file.getPath().replace(rootView.getPath(), ""));
		
		String init = new File(target).getPath();
		for(File file : Utils.scanFolder(init + "/" + rootPackage.getPath()))
			clazz.add(file.getPath().replace(new File(init + "/" + rootPackage.getPath()).getPath(), ""));
		
		for(String view : pml) {
			File file = new File(view);
			String name = file.getName();
			int index = name.lastIndexOf('.');
			name = name.substring(0, index);
			name = Utils.tranformString(name, true, true, false, "");
			File fileClass = new File(file.getParent() + "/" + name + ".class");
			if(!clazz.remove(fileClass.getPath()))
				recompile(file.getPath().substring(1), file);
		}
	}
	
	/**
	 * Compila todos los scripts hayan o no sido compilados anteriormente.
	 */
	public void recompileAll() {
		for(File file : Utils.scanFolder(rootView.getPath()))
			recompile(file.getPath().replace(rootView.getPath(), ""), file);
	}
	///@}

	private Pml recompile(String view, File file) {
		try {
			Set<String> imports = new TreeSet<String>();
			view = view.endsWith("/") || view.endsWith("\\") ? view.substring(0, view.length()-1) : view;
			view = view.startsWith("/") || view.startsWith("\\") ? view.substring(1, view.length()) : view;
			Class<?> clazz = compileSources(view, imports);
			viewsModified.put(view, file.lastModified());
			Pml pml = (Pml) clazz.newInstance();
			views.put(view, pml);
			
			return pml;
		} catch(CompilationError e) {
			throw e;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}


	private Class<?> compileSources(String view, Set<String> imports) throws Exception {
		// Init document
		File input = new File(rootView.getAbsolutePath() + "/" + (Character.isJavaIdentifierStart(view.charAt(0)) ? view : "m" + view));
		List<Node> nodes;
		if(input.getName().endsWith(layoutExtension))
			nodes = Jsoup.parse(Utils.readTextFile(input), input.getAbsolutePath(), Parser.xmlParser()).childNodes();
		else
			nodes = Jsoup.parseBodyFragment(Utils.readTextFile(input)).body().childNodes();
		
		StringBuilder sb = new StringBuilder();
		StringBuilder _source = new StringBuilder();
		
		// Compile pml
		compilePml(sb, _source, nodes, 2, imports);
		resetText(sb, _source, 2);

		// Vars
		String content = _source.toString();
		String name = input.getName();
		int index = name.lastIndexOf('.');
		name = name.substring(0, index);
		name = Character.isJavaIdentifierStart(name.charAt(0)) ? name : "m" + name;
		name = Utils.tranformString(name + " " + input.getName().substring(index+1, input.getName().length()), true, true, false, "");
		File fView = new File(view);
		view = view.replace(fView.getName(), "");
		view = view.endsWith("/") || view.endsWith("\\") ? view.substring(0, view.length()-1) : view;
		view = view.startsWith("/") || view.startsWith("\\") ? view.substring(1, view.length()) : view;
		
		// Construct imports
		String _model = imports.remove("###_model###") ? "\t\tObject _model = null;" : "";
		imports.add("java.util.Map");
		imports.add("com.cubex.pml.Pml");
		String simports = "";
		String now = "";
		String old = "";
		for(String i : imports) {
			index = i.indexOf('.');
			if(index > 0)
				now = i.substring(0, index);
			else
				now = i;
			
			if(!now.equals(old))
				simports += "\n";
			
			simports += "import " + i + ";\n";
			old = now;
		}
		
		// Replace in template
		String source;
		if(view.isEmpty())
			source = template.replace("###package###", rootPackage.getPath().replace("/", ".").replace("\\", "."));
		else
			source = template.replace("###package###", rootPackage.getPath().replace("/", ".").replace("\\", ".") + "." + view.replace("/", ".").replace("\\", "."));
		source = source.replace("###import###", simports);
		source = source.replace("###name###", name);
		source = source.replace("###_model###", _model);
		source = source.replace("###content###", content);
		
		// Compile
		String java = rootPackage.getPath() + "/" + view + "/" + name + ".java";
		createFile(rootSources.getPath(), java, source);
		return compile(rootSources.getPath(), view, java, imports);
	}
	
	public void compilePml(StringBuilder sb, StringBuilder source, List<Node> nodes, int num, Set<String> imports) {
		for(int i=0; i<nodes.size(); i++) {
			Node n = nodes.get(i);
			int j = compilePml(sb, source, nodes, n, num, imports);
			if(j > 0)
				i=j;
		}
	}
	
	public int compilePml(StringBuilder sb, StringBuilder source, List<Node> nodes, Node n, int num, Set<String> imports) {
		List<Node> _nodes = n.childNodes();
		
		boolean line = true;
		Tag tag = null;
		if(n instanceof Element) {
			Element elem = (Element) n;
			
			tag = tags.get(elem.tagName());
			if(org.jsoup.parser.Tag.isKnownTag(elem.tagName()) || tag == null) {
				if(elem.text().isEmpty() && !Pattern.compile("[<][\\/]\\s*" + elem.tagName() + "\\s*[>]").matcher(elem.toString()).find())
					line = false;
				
				if(!org.jsoup.parser.Tag.isKnownTag(elem.tagName()) && tag == null)
					System.err.println("[WARNING]: Tag with name \"" + elem.tagName() + "\" is unknow.");
				
				printTag(sb, source, num, elem, line);
			} else {
				resetText(sb, source, num);
				source.append(tag.compile(elem, num, this, imports));
				resetText(sb, source, num);
				
				List<String> next = tag.nextTags();
				if(next != null && next.size() > 0) {
					if(nodes != null && elem.nextElementSibling() != null && next.contains(elem.nextElementSibling().tagName())) {
						return nodes.indexOf(elem.nextElementSibling())-1;
					}
				}
			}
		}
		
		if(_nodes.size() != 0 && tag == null)
			compilePml(sb, source, _nodes, num, imports);
		
		if(n instanceof Element && line && tag == null) {
			sb.append(printEndTag((Element) n));
		} else if(n instanceof DataNode && !n.toString().isEmpty())
			sb.append(n.toString());
		else if(n instanceof DocumentType && !n.toString().isEmpty())
			sb.append(n.toString());
		else if(n instanceof TextNode && !n.toString().isEmpty())
			sb.append(((TextNode)n).getWholeText().replace("\r\n", "\\n").replace("\n", "\\n").replace("\t", "\\t"));
		else if(n instanceof XmlDeclaration && !n.toString().isEmpty())
			sb.append(n.toString());
		
		if(sb.length() > 80) {
			resetText(sb, source, num);
		}
		
		return -1;
	}

	public void resetText(StringBuilder sb, StringBuilder source, int num) {
		String res = sb.toString();
		res = res.replace("\n", "\\n").replace("\t", "\\t").replace("\"", "\\\"");
		
		if(res.isEmpty())
			return;
		
		source.append(Utils.tabs(num));
		source.append("html.append(\"");
		source.append(res);
		source.append("\");\n");
		
		sb.setLength(0);
	}
	
	private void printTag(StringBuilder sb, StringBuilder source, int num, Element e, boolean line) {
		sb.append("<");
		sb.append(e.tagName());
		
		Pattern exp = Pattern.compile("[$][{][^{}]*[}]");
		for(Attribute attr : e.attributes()) {
			sb.append(" ");
			Matcher m = exp.matcher(attr.getValue()); 
			boolean no = false;
			int end = 0;
			while(m.find()) {
				if(!no)
					sb.append(attr.getKey() + "=\"");
				
				sb.append(attr.getValue().substring(end, m.start()));
				resetText(sb, source, num);
				
				source.append(Utils.tabs(num));
				source.append("html.append(");
				source.append(Tag.convertExpression(attr.getValue().substring(m.start(), m.end())));
				source.append(");\n");
				
				end = m.end();
				no = true;
			}
			
			if(!no)
				sb.append(attr.html());
			else {
				sb.append(attr.getValue().substring(end, attr.getValue().length()));
				sb.append("\"");
			}
		}
		
		if(!line)
			sb.append(" />");
		else
			sb.append(">");
	}
	
	private String printEndTag(Element e) {
		StringBuilder sb = new StringBuilder();
		sb.append("</");
		sb.append(e.tagName());
		sb.append(">");
		return sb.toString();
	}
	
	private void createFile(String dir, String path, String source) throws IOException {
		File sourceFile = new File(dir + "/" + path);
		sourceFile.getParentFile().mkdirs();
		Files.write(sourceFile.toPath(), source.getBytes(StandardCharsets.UTF_8));
	}
	
	private Class<?> compile(String dir, String _package, String source, Set<String> imports) throws Exception {
		File file = new File(dir + "/" + source);
		boolean remove = false;
		
		File targetFolder = new File(target);
		File packageFolder = new File(targetFolder.getPath() + "/" + rootPackage.getPath() + "/" + _package);
		
		Path path1 = Paths.get(file.getPath().replace(".java", ".class"));
		Path path2 = Paths.get(packageFolder.getPath() + "/" + file.getName().replace(".java", ".class"));
		
		if(path2.toFile().exists()) {
			String code = Utils.readTextFile(file);
			String name = file.getName();
			name = name.substring(0, name.length() - 5);
			code = code.replaceFirst(name, name+count);
			code = code.replaceFirst(name+"[(][)]", name+count+"()");
			name += count;
			
			file = new File(file.getParent() + "/" + name + ".java");
			Utils.saveTextFile(file.getPath(), code);
			
			path1 = Paths.get(file.getPath().replace(".java", ".class"));
			path2 = Paths.get(packageFolder.getPath() + "/" + file.getName().replace(".java", ".class"));
			
			count++;
			remove = true;
		}
		
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if(compiler.run(null, out, out, file.getPath()) != 0) {
			if(remove)
				file.delete();
			
			throw new CompilationError("\n" + out.toString(), null);
		}
		
		
		packageFolder.mkdirs();
		Files.move(path1, path2, StandardCopyOption.REPLACE_EXISTING);
		
		String path = file.getPath().replace(file.getName(), "").replace("/", ".").replace("\\", ".");
		path = path.replace(dir + ".", "");
		path = path + file.getName().substring(0, file.getName().length() - 5);
		
		try {
			Class<?> res = defineClass(path, targetFolder.getPath());
			if(remove) {
				Files.delete(path2);
				file.delete();
			}
			
			return res;
		} catch(Exception e) {
			return Class.forName(path);
		}
	}
	
	private Method getMethod(Class<?> clazz, String name, Class<?>... params) {
		try {
			return clazz.getDeclaredMethod("defineClass", params);
		} catch (Exception e) {
			return clazz.getSuperclass() == null ? null : getMethod(clazz.getSuperclass(), name, params);
		}
	}
	
	public Class<?> defineClass(String clazz, String targetFolder) throws Exception {
		try {
			byte[] bytes = loadClassData(new File(targetFolder + "/" + clazz.replace(".", "/") + ".class"));
			Method method = getMethod(getClass().getClassLoader().getClass(), "defineClass", String.class, byte[].class, int.class, int.class);
			method.setAccessible(true);
			Class<?> res = (Class<?>) method.invoke(getClass().getClassLoader(), clazz, bytes, 0, bytes.length);
			return res;
		} catch (IOException ioe) {
			try {
				return ClassLoader.getSystemClassLoader().loadClass(clazz);
			} catch (ClassNotFoundException e) {
			}
		}
		
		return null;
	}
	
	private byte[] loadClassData(File file) throws IOException {
		int size = (int) file.length();
		byte buff[] = new byte[size];
		FileInputStream fis = new FileInputStream(file);
		DataInputStream dis = new DataInputStream(fis);
		dis.readFully(buff);
		dis.close();
		return buff;
	}


	@SuppressWarnings("serial")
	private class Views extends HashMap<String, Pml> {
		
		@Override
		public Pml get(Object view) {
			File file = new File(rootView + "/" + view.toString());
			if(file.exists()) {
				Long modified = viewsModified.get(view);
				if(modified == null)
					return recompile(view.toString(), file);
				else if(modified.compareTo(file.lastModified()) < 0)
					return recompile(view.toString(), file);
				
				Pml pml = super.get(view);
				if(pml != null && !alwaysCompile)
					return pml;
				else
					return recompile(view.toString(), file);
					
			} else
				return null;
		}
		
	}
	
	
	private static final String template = "package ###package###;\n"
			+ "###import###\n"
			+ "public class ###name### implements Pml {\n"
			+ "\n"
			+ "\tpublic ###name###() {\n"
			+ "\t\tsuper();\n"
			+ "\t}\n"
			+ "\n"
			+ "\tpublic String exec(Map<String, Object> model, Map<String, Pml> views, Map<String, Pml> tiles) {\n"
			+ "\t\tStringBuilder html = new StringBuilder();\n"
			+ "###_model###"
			+ "\t\t\n"
			+ "###content###"
			+ "\t\t\n"
			+ "\t\treturn html.toString();\n"
			+ "\t}\n"
			+ "\n"
			+ "}\n";
}
