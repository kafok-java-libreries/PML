package com.cubex.pml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Expression {

	public static Object solve(Context cxt, String expression) {
		List<Token> _tokens = tokenizer(expression);
		_tokens = new ArrayList<Token>(_tokens);
		List<Token> tokens = new LinkedList<Token>();
		
		preprocess(tokens, _tokens, 0, false);
		
		return createAST(cxt, tokens, 0, tokens.size());
	}
	
	private static Object createAST(Context cxt, List<Token> tokens, int init, int end) {
		Token left = null;
		Token right = null;
		Operator op = null;
		boolean first = true;
		
		for(int i=init; i<tokens.size() && i<end; i++) {	//TODO Algun dia
//			Token token = tokens.get(i);
//			if(TokenType.isValue(token.getType())) {
//				if(first) {
//					left = token;
//					first = false;
//				} else 
//					right = token;
//			} else if(TokenType.isOperator(token.getType())) {
//				op = ops.get(token.getToken());
//			} else if(TokenType.isBracket(token.getType())) {
//				int j = findBracket(tokens, i+1);
//				if(left == null)
//					left = createAST(cxt, tokens, i+1, j);
//				else
//					right = createAST(cxt, tokens, i+1, j);
//				
//				i = j;
//			}
//			
//			if(right != null) {
//				left = op.solve(cxt, left, right);
//				right = null;
//			} else if(op != null && op.isUnary() && !first) {
//				left = op.solve(cxt, left);
//			}
		}
		
		return left;
	}
	
	private static int findBracket(List<Token> tokens, int init) {
		int open = 1;
		int i;
		for(i=init; i<tokens.size() && open!=0; i++) {
			Token token = tokens.get(i);
			if(token.getType().equals(TokenType.START_BRACKET))
				open++;
			else if(token.getType().equals(TokenType.END_BRACKET))
				open--;
		}
		
		return i-1 < 0 ? 0 : i-1;
	}

	private static Object getTypeToken(Context cxt, Token token) {
		Object res = null;
		switch(token.getType()) {
			case INT_LITERAL:
				try {
					res = new Integer(token.getToken());
				} catch(NumberFormatException e3) {
					try {
						res = new Long(token.getToken());
					} catch(NumberFormatException e4) {
						//TODO Error
					}
				}
				break;
			case REAL_LITERAL:
				try {
					res = new Double(token.getToken());
				} catch(NumberFormatException e2) {
					//TODO Error
				}
				break;
			case IDENTIFIER:
				res = cxt.getVariable(token.getToken());
				break;
			case LITERAL:
				if(token.getToken().equals("null"))
					res = "null";
				break;
			default:
				res = token.getToken();
				break;
		}
		
		return res;
	}
	
 	private static int preprocess(List<Token> res, List<Token> tokens, int init, boolean _open) {
		int i;
		if(!_open) {
			for(i=0; i<tokens.size(); i++) {
				Token token = tokens.get(i);
				if(token.getType().equals(TokenType.START_OPERATOR)) {
					tokens.add(i+1, new Token("(", TokenType.START_BRACKET));
					i++;
				} else if(token.getType().equals(TokenType.END_OPERATOR))
					tokens.set(i, new Token(")", TokenType.END_BRACKET));
			}
		}
		
		Stack<Integer> priority = new Stack<Integer>();
		int open = _open ? 1 :0;
		boolean unary = false;
		for(i=init; i<tokens.size(); i++) {
			Token token = tokens.get(i);
			if(token.getType().equals(TokenType.START_BRACKET)) {
				res.add(new Token("(", TokenType.START_BRACKET));
				i = preprocess(res, tokens, i+1, true);
			} else if(token.getType().equals(TokenType.END_BRACKET)) {
				break;
			} else {
				if(TokenType.isOperator(token.getType())) {
					Operator op = ops.get(token.getToken());	//TODO Error operador no reconocido;
					
					if(op.isUnary()) {
						res.add(new Token("(", TokenType.START_BRACKET));
						res.add(token);
						res.add(tokens.get(i+1));//TODO Error comprobar q sea valor
						res.add(new Token(")", TokenType.END_BRACKET));
						i++;
						unary = true;
						continue;
					}
					
					if(priority.isEmpty() || priority.peek() < op.getPriority()) {
						if(i>0 && tokens.get(i-1).getType().equals(TokenType.END_BRACKET)) 
							res.add(token);
						else {
							for(int j=(priority.isEmpty() ? op.getPriority() : priority.peek()); j<op.getPriority(); j++) {
								if(unary)
									res.add(res.size()-4, new Token("(", TokenType.START_BRACKET));
								else
									res.add(new Token("(", TokenType.START_BRACKET));
								open++;
							}
							
							priority.push(op.getPriority());
							
							if(!unary && !tokens.get(i-1).getType().equals(TokenType.END_BRACKET))
								res.add(tokens.get(i-1));
							res.add(token);
						}
					} else if(priority.peek() > op.getPriority()) {
						if(!unary && !tokens.get(i-1).getType().equals(TokenType.END_BRACKET))
							res.add(tokens.get(i-1));
						
						while(!priority.isEmpty() && open>0 && priority.peek() > op.getPriority()) {
							res.add(new Token(")", TokenType.END_BRACKET));
							open--;
							priority.pop();
						}
						
						res.add(token);
					} else {
						if(!unary && !tokens.get(i-1).getType().equals(TokenType.END_BRACKET))
							res.add(tokens.get(i-1));
							
						res.add(token);
					}

					unary = false;
				}
			}
		}
		
		if(TokenType.isValue(tokens.get(i-1).getType()) && !unary)
			res.add(tokens.get(i-1));
		
		for(int j=0; j<open; j++)
			res.add(new Token(")", TokenType.END_BRACKET));
		
		return i;
	}

	private static List<Token> tokenizer(String expression) {
		//TODO comprobar el numero de parentesis
		List<Token> tokens = new LinkedList<Token>();
		StringBuilder token = new StringBuilder();
		TokenType search = TokenType.NONE;
		boolean stringLiteral = false;
		Token last = null;
		for(int i=0; i<expression.length(); i++) {
			char c = expression.charAt(i);
			
			if(Character.isWhitespace(c) && !stringLiteral) {
				last = resetToken(tokens, token, search);
				search = TokenType.NONE;
				continue;
			} else if(c == '(') {
				last = resetToken(tokens, token, search);
				tokens.add(new Token(""+c, TokenType.START_BRACKET));
				search = TokenType.NONE;
				continue;
			} else if(c == ')') {
				last = resetToken(tokens, token, search);
				tokens.add(new Token(""+c, TokenType.END_BRACKET));
				search = TokenType.NONE;
				continue;
			}
			
			switch(search) {
				case IDENTIFIER:	// Identifier
					if(!Character.isJavaIdentifierPart(c)) {
						last = resetToken(tokens, token, search);
						search = TokenType.NONE;
					}
					
					break;
					
				case START_OPERATOR:
				case END_OPERATOR:
				case OPERATOR:	// Operator
					if(Character.isJavaIdentifierStart(c) || Character.isDigit(c) || c == '\'' ||
							c == '.' && expression.length() > i+1 && Character.isDigit(expression.charAt(i+1))) {
						last = resetToken(tokens, token, search);
						search = TokenType.NONE;
					}
					
					break;
					
				case LITERAL:	// Literal
					if(stringLiteral) {
						if(c == '\'') {
							last = resetToken(tokens, token, TokenType.STRING_LITERAL);
							search = TokenType.NONE;
						}
					} else if(!Character.isDigit(c)) {
						if((c != '-' && c != '.') || token.toString().contains(""+c) || (c == '-' && token.length() > 0)) {
							last = resetToken(tokens, token, token.toString().contains(".") ? TokenType.REAL_LITERAL : TokenType.INT_LITERAL);
							search = TokenType.NONE;
						}
					}
					
					break;
					
				default:
					break;
			}
			
			if(search.equals(TokenType.NONE)) {
				if(Character.isWhitespace(c))
					continue;
				
				if(Character.isJavaIdentifierStart(c))
					search = TokenType.IDENTIFIER;
				else if(Character.isDigit(c) || c == '\'' || 
						(c == '.' && expression.length() > i+1 && Character.isDigit(expression.charAt(i+1))) || 
						((c == '-' && expression.length() > i+1 && Character.isDigit(expression.charAt(i+1))) && (last == null || TokenType.isOperator(last.getType())))) {
					search = TokenType.LITERAL;
					if(c == '\'') {
						if(stringLiteral) {
							search = TokenType.NONE;
							stringLiteral = false;
						} else
							stringLiteral = true;
						
						continue;
					}
				} else if(c == '[') {
					search = TokenType.START_OPERATOR;
					token.append(c);
					last = resetToken(tokens, token, search);
					search = TokenType.NONE;
					continue;
				} else if(c == ']') {
					search = TokenType.END_OPERATOR;
					token.append(c);
					last = resetToken(tokens, token, search);
					search = TokenType.NONE;
					continue;
				} else
					search = TokenType.OPERATOR;
			}
			
			token.append(c);
		}

		resetToken(tokens, token, Character.isJavaIdentifierStart(token.charAt(0)) ? TokenType.IDENTIFIER : (stringLiteral ? TokenType.STRING_LITERAL : (token.toString().contains(".") ? TokenType.REAL_LITERAL : TokenType.INT_LITERAL)));
		return tokens;
	}

	private static Token resetToken(List<Token> tokens, StringBuilder token, TokenType search) {
		Token res = null;
		String t = token.toString();
		
		if(t.length() > 0) {
			if(t.charAt(0) == '-' || Character.isDigit(t.charAt(0))) {
				if(t.length() == 1 && Character.isDigit(t.charAt(0)))
					search = TokenType.INT_LITERAL;
				
				for(int i=1; i<t.length(); i++) {
					if(!Character.isDigit(t.charAt(i)) && t.charAt(0) == '.') {
						search = TokenType.STRING_LITERAL;
						break;
					}
				}
				
				if(search.equals(TokenType.INT_LITERAL) && t.contains("."))
					search = TokenType.REAL_LITERAL;
			}
			
			if(t.equals("true") || t.equals("false") || t.equals("null"))
				search = TokenType.LITERAL;
			
			res = new Token(t, search);
			tokens.add(res);
			token.setLength(0);
		}
		
		return res;
	}
	
	
	private static interface Operator {
		Object solve(Context cxt, Object... obj);
		int getPriority();
		boolean isUnary();
		boolean isStructural();
	}
	
	private static class Token {
		private String token;
		private TokenType type;
		
		private Token(String token, TokenType type) {
			super();
			this.token = token;
			this.type = type;
		}

		
		public String getToken() {
			return token;
		}

		public TokenType getType() {
			return type;
		}
		
		public String toString() {
			return getToken() + " - " + getType();
		}
	}
	
	private static enum TokenType {
		OPERATOR,
		START_OPERATOR,
		END_OPERATOR,
		IDENTIFIER,
		LITERAL,
		INT_LITERAL,
		REAL_LITERAL,
		STRING_LITERAL,
		START_BRACKET,
		END_BRACKET,
		NONE;
		
		public static boolean isOperator(TokenType token) {
			return token.equals(OPERATOR) || token.equals(START_OPERATOR) || token.equals(END_OPERATOR);
		}
		
		public static boolean isValue(TokenType token) {
			return token.equals(IDENTIFIER) || token.equals(LITERAL) || token.equals(INT_LITERAL) || token.equals(REAL_LITERAL) || 
					token.equals(STRING_LITERAL);
		}
		
		public static boolean isBracket(TokenType token) {
			return token.equals(START_BRACKET) || token.equals(END_BRACKET);
		}
	}
	
	
	private static Map<String, Operator> ops;
	
	static {
		ops = new HashMap<String, Operator>();
		ops.put("+", new OperatorPlus());
		ops.put("-", new OperatorMinus());
		ops.put("*", new OperatorMultiplication());
		ops.put("/", new OperatorDivision());
		ops.put("%", new OperatorModule());
		ops.put(">", new OperatorHigher());
		ops.put(">=", new OperatorHigherThan());
		ops.put("<", new OperatorLess());
		ops.put("<=", new OperatorLessThan());
		ops.put("==", new OperatorEquals());
		ops.put("!=", new OperatorNotEquals());
		ops.put("&&", new OperatorAnd());
		ops.put("||", new OperatorOr());
		ops.put("!", new OperatorNot());
	}
	
	
	private static class OperatorPlus implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof String || obj[1] instanceof String)
				return obj[0].toString() + obj[1].toString();
			else if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() + ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() + ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() + ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorMinus implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() - ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() - ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() - ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorMultiplication implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() * ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() * ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() * ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 2;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	
	private static class OperatorDivision implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() / ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() / ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() / ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 2;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorModule implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() % ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() % ((Number)obj[1]).intValue();
//			else	%%TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 2;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorHigher implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() > ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() > ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() > ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorHigherThan implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() >= ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() >= ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() >= ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorLess implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() < ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() < ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() < ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorLessThan implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[0] instanceof Double || obj[1] instanceof Double)
				return ((Number)obj[0]).doubleValue() <= ((Number)obj[1]).doubleValue();
			else if(obj[0] instanceof Long || obj[1] instanceof Long)
				return ((Number)obj[0]).longValue() <= ((Number)obj[1]).longValue();
			else if(obj[0] instanceof Integer || obj[1] instanceof Integer)
				return ((Number)obj[0]).intValue() <= ((Number)obj[1]).intValue();
//			else	//TODO Lanzar error
//			return
		
			return null;
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorEquals implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			if(obj[1].toString().equals("null"))
				obj[1] = null;
			
			if(obj[0] == null || obj[1] == null)
				return obj[0] == obj[1];
			
			return obj[0].equals(obj[1]);
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorNotEquals implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			return !obj[0].equals(obj[1]);
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorAnd implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			return (new Boolean(obj[0].toString())) && (new Boolean(obj[1].toString()));
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorOr implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			return (new Boolean(obj[0].toString())) || (new Boolean(obj[1].toString()));
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return false;
		}

		public boolean isStructural() {
			return false;
		}
	}
	

	private static class OperatorNot implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			obj[0] = getTypeToken(cxt, (Token) obj[0]);
			obj[1] = getTypeToken(cxt, (Token) obj[1]);
			
			return !(new Boolean((String)obj[0]));
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return true;
		}

		public boolean isStructural() {
			return false;
		}
	}
	
	private static class OperatorPoint implements Operator {
		
		public Object solve(Context cxt, Object... obj) {
			return !(new Boolean((String)obj[0]));
		}
		
		public int getPriority() {
			return 1;
		}

		public boolean isUnary() {
			return true;
		}

		public boolean isStructural() {
			return true;
		}
	}
}
