package com.cubex.pml.exceptions;

@SuppressWarnings("serial")
public class ParseException extends RuntimeException {

	public ParseException() {
		super();
	}
	
	public ParseException(String msg) {
		super(msg);
	}
	
}
