package com.cubex.pml;

import org.jsoup.nodes.Element;

/**
 * Error de compilación de un script PML. Puede lanzarse por una sintaxis incorrecta en una etiqueta, por una expresión malformada o
 * un mal uso de las etiquetas puede haber compilado un script java con errores de compilación.
 */
public class CompilationError extends RuntimeException {

	private static final long serialVersionUID = 3492974978531074873L;

	/**
	 * Constructor con mensaje de error y elemento que lo ocasionó.
	 * @param msg Mensaje de error de compilación.
	 * @param e Elemento que lo ocasionó, puede ser nulo si el error proviene del script java.
	 */
	public CompilationError(String msg, Element e) {
		super(e != null ? msg + "\n\n\t" + e.outerHtml().replace("\n", "\n\t") + "\n" : msg);
	}
	
}
