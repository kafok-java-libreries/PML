package com.cubex.pml;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

/**
 * Etiqueta PML. El lenguaje PML consiste en un conjunto de etiquetas que extienden HTML, y las cuales
 * se renderizan en el servidor, dando como resultado un HTML din�mico como en otros lenguajes como PHP,
 * JSP o ASP. Esta clase representa una etiqueta PML, y es la encargada de compilar la etiqueta en c�digo
 * java el cual se incluir� en el script generador de la vista.
 * 
 * @nosubgrouping
 */
public abstract class Tag {
	
	/**
	 * Compila una etiqueta concreta. Este m�todo es el encargado de compilar una etiqueta concreta. Devuelve 
	 * una cadena resultante de dicha compilaci�n.
	 * 
	 * @param elem Elemento que se quiere compilar.
	 * @param tabs N�mero de tabulaciones al generar el c�digo java.
	 * @param manager Compilador del script que queremos generar.
	 * @param imports Conjunto de clases importadas que ser�n traducidos en imports de java.
	 * @return Cadena resultante de la compilaci�n.
	 */
	public abstract String compile(Element elem, int tabs, PmlCompileManager manager, Set<String> imports);
	
	
	/**
	 * @name Propiedades
	 */
	///@{
	
	/**
	 * Devuelve el nombre de la etiqueta.
	 * @return Nombre de la etiqueta.
	 */
	public abstract String getName();
	
	/**
	 * Devuelve si la etiqueta puede tener contenido.
	 * @return Devuelve si la etiqueta puede tener contenido.
	 */
	public abstract boolean canBlock();
	
	/**
	 * Devuelve si la etiqueta puede auto-cerrarse.
	 * @return Devuelve si la etiqueta puede auto-cerrarse.
	 */
	public abstract boolean canEmpty();
	
	/**
	 * Devuelve una lista de etiquetas que pueden preceder a esta. En caso de que existan dichas etiquetas,
	 * se buscar� la siguiente etiqueta y si coincide con alguna de estas, se compilar�n seguidas, ignorando el
	 * contenido textual intermedio.
	 * @return Lista de etiquetas que pueden preceder a esta.
	 */
	public abstract List<String> nextTags();
	///@}
	
	
	protected static void checkValidExpression(Element e, String exp, boolean required) {
		if(required || !exp.isEmpty()) {
			if(!checkIsExpression(exp))
				throw new CompilationError("Invalid expression: \"" + exp + "\".", e);
		}
	}

	protected static boolean checkIsExpression(String exp) {
		return exp.startsWith("${") && exp.endsWith("}");
	}
	
	protected static void checkValidExpression(Element e, String exp) {
		checkValidExpression(e, exp, true);
	}
	
	protected static void checkRequireAttribute(Element e, String attr, String value) {
		if(value == null || value.isEmpty())
			throw new CompilationError("The attribute \"" + attr + "\" is required.", e);
	}
	
	protected static void checkAnyRequireAttributes(Element e, String... attrs) {
		boolean res = false;
		String fail = "";
		for(int i=0; i<attrs.length; i+=2) {
			fail += "\t-> " + attrs[i] + " (is missing)\n";
			if(attrs[i+1] != null || !attrs[i+1].isEmpty()) {
				res = true;
				break;
			}
		}
		
		if(!res)
			throw new CompilationError("Any of the following attributes is required:\n" + fail, e);
	}
	
	protected static boolean checkIsString(String str) {
		return !str.isEmpty() && str.startsWith("'") && str.endsWith("'");
	}
	
	protected static String convertExpression(String exp) {
		if(!exp.isEmpty())
			return exp.substring(2, exp.length()-1).replace("'", "\"");
		else
			return exp;
	}
	
	protected static String convertString(String str) {
		if(!str.isEmpty())
			return str.substring(1, str.length()-1);
		else
			return str;
	}
	
	protected void check(Element e) {
		if(e.childNodeSize() > 0 && !canBlock())
			throw new CompilationError("The tag <" + getName() + "> can't contain body.", e);
		
		if(e.childNodeSize() == 0 && !canEmpty())
			throw new CompilationError("The tag <" + getName() + "> must have body.", e);
	}
	
}
