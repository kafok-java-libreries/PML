package com.cubex.pml;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Context {

	private Stack<Map<String, Object>> stack;

	public Context() {
		super();
		stack = new Stack<Map<String, Object>>();
		push();
	}
	
	public void push() {
		Map<String, Object> map = new HashMap<String, Object>();
		stack.push(map);
	}
	
	public void pop() {
		stack.pop();
	}
	
	public Object getVariable(String name) {
		for(int i=stack.size()-1; i>=0; i--) {
			Map<String, Object> map = stack.get(i);
			if(map != null) {
				Object res = map.get(name);
				if(res != null)
					return res;
			}
		}
		
		return null;
	}
	
	public void setVariable(String name, Object value) {
		stack.peek().put(name, value);
	}
}
