package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class DeclareTag extends Tag {

	public String getName() {
		return "declare";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String type = e.attr("type");
		type = type.isEmpty() ? "String" : type;
		String var = e.attr("var");
		String value = e.attr("value");
		
		checkRequireAttribute(e, "var", var);
		checkRequireAttribute(e, "value", value);
		if(checkIsExpression(value))
			value = convertExpression(value);
		else if(type.equals("String"))
			value = "\"" + value + "\"";
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append(type);
		source.append(" ");
		source.append(var);
		source.append(" = ");
		source.append(value);
		
		source.append(";\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
