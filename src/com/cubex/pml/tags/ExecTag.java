package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class ExecTag extends Tag {

	public String getName() {
		return "exec";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String exp = e.attr("exp");
		
		checkRequireAttribute(e, "exp", exp);
		checkValidExpression(e, exp);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append(convertExpression(exp));
		source.append(";\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}
	
	public List<String> nextTags() {
		return null;
	}

}
