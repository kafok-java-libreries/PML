package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class TileTag extends Tag {

	public String getName() {
		return "tile";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String name = e.attr("name");
		
		checkRequireAttribute(e, "name", name);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("html.append(tiles.get(\"");
		source.append(name);
		source.append("\").exec(model, views, tiles));\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}
	
	public List<String> nextTags() {
		return null;
	}

}
