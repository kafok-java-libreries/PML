package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.CompilationError;
import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class ForLoopTag extends Tag {

	public String getName() {
		return "forloop";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String var = e.attr("var");
		String begin = e.attr("begin");
		String end = e.attr("end");
		String step = e.attr("step");
		
		begin = begin.isEmpty() ? "0" : begin;
		step = step.isEmpty() ? "1" : step;
		
		checkRequireAttribute(e, "var", var);
		checkRequireAttribute(e, "end", end);
		
		String sim = "<";
		boolean send = true;
		boolean sstep = true;
		try {
			boolean nums = false;
			int a=0;
			int b=0;
			int c=0;
			
			if(!checkIsExpression(begin)) {
				a = new Integer(begin);
				nums = true;
			} else
				begin = convertExpression(begin);
			
			if(!checkIsExpression(end)) {
				b = new Integer(end);
				nums = true;
				send = false;
			} else
				end = convertExpression(end);
			
			if(!checkIsExpression(step)) {
				c = new Integer(step);
				nums = true;
				sstep = false;
			} else
				step = convertExpression(step);
			
			if(nums && (a > b && c < 0))
				sim = ">";
		} catch(Throwable exc) {
			throw new CompilationError(exc.getLocalizedMessage(), e);
		}
		
		
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("for(int ");
		source.append(var);
		source.append("=");
		source.append(begin);
		source.append("; ");
		
		if(send)
			source.append(end);
		else {
			source.append(var);
			source.append(sim);
			source.append(end);
		}
		
		source.append("; ");
		
		if(sstep)
			source.append(step);
		else {
			source.append(var);
			source.append("+=");
			source.append(step);
		}
		
		source.append(") {\n");
		
		manager.compilePml(sb, source, e.childNodes(), tabs+1, imports);
		manager.resetText(sb, source, tabs+1);
		
		source.append(stab);
		source.append("}\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}

	public List<String> nextTags() {
		return null;
	}

}
