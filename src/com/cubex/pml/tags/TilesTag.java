package com.cubex.pml.tags;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class TilesTag extends Tag {

	public String getName() {
		return "tiles";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String layout = e.attr("layout");
		
		checkRequireAttribute(e, "layout", layout);
		
		imports.add("java.util.HashMap");
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("tiles = new HashMap<String, Pml>();\n");
		
		for(Node n : e.childNodes()) {
			if(!(n instanceof Element && allowChildren().contains(((Element)n).tagName())))
				continue;
			
			manager.compilePml(sb, source, null, n, tabs, imports);
			manager.resetText(sb, source, tabs);
		}
		
		source.append(stab);
		source.append("html.append(views.get(\"");
		source.append(layout);
		source.append("\").exec(model, views, tiles));\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}
	
	public List<String> nextTags() {
		return null;
	}
	
	public List<String> allowChildren() {
		return Arrays.asList("put", "set");
	}

}
