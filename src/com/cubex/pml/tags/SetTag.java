package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class SetTag extends Tag {

	public String getName() {
		return "set";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String var = e.attr("var");
		String exp = e.attr("exp").replace("'", "\"");
		String value = e.attr("value");
		boolean global = e.hasAttr("global");
		
		boolean string = checkIsString(value);
		
		checkRequireAttribute(e, "var", var);
		checkAnyRequireAttributes(e, "exp", exp, "value", value);
		checkValidExpression(e, exp, false);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		
		if(global) {
			source.append("model.put(\"");
			source.append(var);
			source.append("\", ");
		} else {
			source.append(var);
			source.append(" = ");
		}
		
		if(string) {
			source.append("\"");
			value = convertString(value);
		}
		
		if(!value.isEmpty())
			source.append(value);
		else if(!exp.isEmpty())
			source.append(convertExpression(exp));
			
		if(string)
			source.append("\"");
		
		if(global)
			source.append(")");
			
		source.append(";\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
