package com.cubex.pml.tags;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class SwitchTag extends Tag {

	public String getName() {
		return "switch";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String exp = e.attr("exp");
		String var = e.attr("var");
		
		checkAnyRequireAttributes(e, "exp", exp, "var", var);
		checkValidExpression(e, exp, false);
		exp = convertExpression(exp);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("switch(");
		source.append(exp.isEmpty() ? var : exp);
		source.append(") {\n");
		
		for(Node n : e.childNodes()) {
			if(!(n instanceof Element && allowChildren().contains(((Element)n).tagName())))
				continue;
			
			manager.compilePml(sb, source, null, n, tabs+1, imports);
			manager.resetText(sb, source, tabs+1);
		}
		
		source.append(stab);
		source.append("}\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}
	
	public List<String> nextTags() {
		return null;
	}
	
	public List<String> allowChildren() {
		return Arrays.asList("case", "default");
	}

}
