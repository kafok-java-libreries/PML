package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;

public class ImportTag extends Tag {

	public String getName() {
		return "import";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String clazz = e.attr("class");
		
		checkRequireAttribute(e, "class", clazz);
		
		imports.add(clazz);
		source.setLength(0);
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
