package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class OutTag extends Tag {

	public String getName() {
		return "out";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String exp = e.attr("exp");
		String var = e.attr("var");
		
		checkAnyRequireAttributes(e, "exp", exp, "var", var);
		checkValidExpression(e, exp, false);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("html.append(");
		if(!var.isEmpty())
			source.append(var);
		else if(!exp.isEmpty())
			source.append(convertExpression(exp));
		
		source.append(");\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
