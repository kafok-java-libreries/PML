package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class IncludeTag extends Tag {

	public String getName() {
		return "include";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String view = e.attr("view");
		
		checkRequireAttribute(e, "view", view);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("html.append(views.get(\"");
		source.append(view);
		source.append("\").exec(model, views, tiles));\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
