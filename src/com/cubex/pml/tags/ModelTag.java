package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class ModelTag extends Tag {

	public String getName() {
		return "model";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String type = e.attr("type");
		String var = e.attr("var");
		
		checkRequireAttribute(e, "var", var);
		checkRequireAttribute(e, "type", type);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("_model = model.get(\"");
		source.append(var);
		source.append("\");\n");
		
		source.append(stab);
		source.append(type);
		source.append(" ");
		source.append(var);
		source.append(" = _model != null ? (");
		source.append(type);
		source.append(") _model : null");
		source.append(";\n");
		
		imports.add("###_model###");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}
	
	public List<String> nextTags() {
		return null;
	}

}
