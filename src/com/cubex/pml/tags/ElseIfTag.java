package com.cubex.pml.tags;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class ElseIfTag extends Tag {

	public String getName() {
		return "else-if";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String test = e.attr("test");
		
		checkRequireAttribute(e, "test", test);
		checkValidExpression(e, test);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("else if(");
		source.append(convertExpression(test));
		source.append(") {\n");
		
		manager.compilePml(sb, source, e.childNodes(), tabs+1, imports);
		manager.resetText(sb, source, tabs+1);
		
		source.append(stab);
		source.append("}\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}

	public List<String> nextTags() {
		return Arrays.asList("else");
	}

}
