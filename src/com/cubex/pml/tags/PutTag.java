package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class PutTag extends Tag {

	public String getName() {
		return "put";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder source = new StringBuilder();
		
		String name = e.attr("name");
		String value = e.attr("value");
		
		checkRequireAttribute(e, "name", name);
		checkRequireAttribute(e, "value", value);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("tiles.put(\"");
		source.append(name);
		source.append("\", views.get(\"");
		source.append(value);
		source.append("\"));\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return false;
	}

	public boolean canEmpty() {
		return true;
	}

	public List<String> nextTags() {
		return null;
	}

}
