package com.cubex.pml.tags;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class CatchTag extends Tag {

	public String getName() {
		return "catch";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String exception = e.attr("exception");
		String var = e.attr("var");
		
		if(exception.isEmpty() && var.isEmpty()) {
			exception = "Throwable";
			var = "e";
		} else {
			checkRequireAttribute(e, "exception", exception);
			checkRequireAttribute(e, "var", var);
		}
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("catch(");
		source.append(exception);
		source.append(" ");
		source.append(var);
		source.append(") {\n");
		
		manager.compilePml(sb, source, e.childNodes(), tabs+1, imports);
		manager.resetText(sb, source, tabs+1);
		
		source.append(stab);
		source.append("}\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}
	
	public List<String> nextTags() {
		return Arrays.asList("catch");
	}

}
