package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class ForEachTag extends Tag {

	public String getName() {
		return "foreach";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String var = e.attr("var");
		String items = e.attr("items");
		String type = e.attr("type");
		String index = e.attr("index");
		
		checkRequireAttribute(e, "var", var);
		checkRequireAttribute(e, "items", items);
		checkRequireAttribute(e, "type", type);
		
		checkIsExpression(items);
		items = convertExpression(items);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		
		if(!index.isEmpty()) {
			source.append("int ");
			source.append(index);
			source.append(" = 0;\n");
			source.append(stab);
		}
		
		source.append("for(");
		source.append(type);
		source.append(" ");
		source.append(var);
		source.append(" : ");
		source.append(items);
		source.append(") {\n");
		
		manager.compilePml(sb, source, e.childNodes(), tabs+1, imports);
		manager.resetText(sb, source, tabs+1);
		
		if(!index.isEmpty()) {
			source.append("\n");
			source.append(index);
			source.append("++;\n");
			source.append(stab);
		}
		
		source.append(stab);
		source.append("}\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}

	public List<String> nextTags() {
		return null;
	}

}
