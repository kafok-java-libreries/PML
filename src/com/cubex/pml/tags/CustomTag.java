package com.cubex.pml.tags;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class CustomTag extends Tag {

	private String name;
	private List<Attribute> attributes;
	private String content;
	private boolean canBlock;
	private boolean canEmpty;
	
	public CustomTag(String tag) {
		super();
		attributes = new LinkedList<Attribute>();
		canBlock = false;
		canEmpty = true;
		
		List<Element> elems = Jsoup.parseBodyFragment(tag).body().children();
		for(Element elem : elems) {
			String value;
			String name;
			boolean required;
			String valueDefault;
			switch(elem.tagName()) {
				case "name":
					value = elem.attr("value");
					checkRequireAttribute(elem, "value", value);
					
					this.name = value;
					break;
					
				case "attribute":
					name = elem.attr("name");
					required = elem.hasAttr("required");
					valueDefault = elem.attr("default");
					
					checkRequireAttribute(elem, "name", name);
					checkAnyRequireAttributes(elem, "required", ""+required, "default", valueDefault);
					
					if(required)
						attributes.add(new Attribute(name));
					else
						attributes.add(new Attribute(name, valueDefault));
						
					break;
					
				case "content":
					StringBuilder contentTag = new StringBuilder();
					for(Node node : elem.childNodes()) {
						if(node instanceof TextNode)
							contentTag.append(((TextNode)node).getWholeText());
						else
							contentTag.append(node.toString());
					}
					content = contentTag.toString();
					
					break;
					
				case "canblock":
					value = elem.attr("value");
					checkRequireAttribute(elem, "value", value);
					
					canBlock = new Boolean(value);
					break;
					
				case "canempty":
					value = elem.attr("value");
					checkRequireAttribute(elem, "value", value);
					
					canEmpty = new Boolean(value);
					break;
			}
		}
	}

	public String getName() {
		return name;
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();

		String res = content;
		String stab = Utils.tabs(tabs);
		
		for(Attribute attr : attributes) {
			if(e.hasAttr(attr.getName()) && e.attr(attr.getName()).isEmpty())
				e.attr(attr.getName(), "true");
			
			if(attr.isRequired())
				checkRequireAttribute(e, attr.getName(), e.attr(attr.getName()));
			else if(e.attr(attr.getName()).isEmpty())
				e.attr(attr.getName(), attr.getValueDefault());
				
			res = stab + "<declare type=\"String\" var=\"" + attr.getName() + "\" value=\"" + e.attr(attr.getName()) + "\" />\n" + res;
		}
		
		if(canBlock) {
			StringBuilder contentTag = new StringBuilder();
			for(Node node : e.childNodes()) {
				if(node instanceof TextNode)
					contentTag.append(((TextNode)node).getWholeText());
				else
					contentTag.append(node.toString());
			}
			res = res.replaceAll("<dobody[ ]*\\/>", contentTag.toString());
		}
		
		manager.compilePml(sb, source, Jsoup.parseBodyFragment(res).body().childNodes(), tabs, imports);
		manager.resetText(sb, source, tabs);
		
		return source.toString();
	}

	public boolean canBlock() {
		return canBlock;
	}

	public boolean canEmpty() {
		return canEmpty;
	}
	
	public List<String> nextTags() {
		return null;
	}

	
	private static class Attribute {
		private String name;
		private boolean required;
		private String valueDefault;
		
		public Attribute(String name, String valueDefault) {
			super();
			this.name = name;
			this.required = false;
			this.valueDefault = valueDefault;
		}
		
		public Attribute(String name) {
			super();
			this.name = name;
			this.required = true;
			this.valueDefault = null;
		}

		public String getName() {
			return name;
		}

		public boolean isRequired() {
			return required;
		}

		public String getValueDefault() {
			return valueDefault;
		}
	}
}
