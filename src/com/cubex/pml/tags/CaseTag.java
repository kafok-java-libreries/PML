package com.cubex.pml.tags;

import java.util.List;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.cubex.pml.PmlCompileManager;
import com.cubex.pml.Tag;
import com.cubex.pml.Utils;

public class CaseTag extends Tag {

	public String getName() {
		return "case";
	}

	public String compile(Element e, int tabs, PmlCompileManager manager, Set<String> imports) {
		check(e);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder source = new StringBuilder();
		
		String value = e.attr("value");
		String text = e.attr("text");
		
		checkAnyRequireAttributes(e, "value", value, "text", text);
		
		String stab = Utils.tabs(tabs);
		source.append(stab);
		source.append("case ");
		source.append(value.isEmpty() ? "\"" + text + "\"" : value);
		source.append(":\n");
		
		manager.compilePml(sb, source, e.childNodes(), tabs+1, imports);
		manager.resetText(sb, source, tabs+1);
		
		source.append(Utils.tabs(tabs+1));
		source.append("break;\n");
		
		return source.toString();
	}

	public boolean canBlock() {
		return true;
	}

	public boolean canEmpty() {
		return false;
	}
	
	public List<String> nextTags() {
		return null;
	}

}
