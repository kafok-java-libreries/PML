package com.cubex.pml;

import java.util.Map;

/**
 * Script generador de vistas. Esta interfaz representa un script generador de una vista, y es el resultado
 * de compilar archivos PML.
 */
public interface Pml {
	
	/**
	 * Ejecuta el script y genera una vista como resultado.
	 * @param model Variables globales de la vista.
	 * @param views Conjunto de vistas existentes, se utilizará para la modulación de las páginas.
	 * @param tiles Conjunto de tiles.
	 * @return Vista resultante.
	 */
	String exec(Map<String, Object> model, Map<String, Pml> views, Map<String, Pml> tiles);
	
}
